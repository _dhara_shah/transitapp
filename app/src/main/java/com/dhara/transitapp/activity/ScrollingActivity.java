package com.dhara.transitapp.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.dhara.transitapp.R;
import com.dhara.transitapp.adapter.MyPagerAdapter;
import com.dhara.transitapp.callback.IFragmentLoadListener;
import com.dhara.transitapp.manager.SessionManager;
import com.dhara.transitapp.model.Segments;
import com.dhara.transitapp.model.TransitRoutes;
import com.dhara.transitapp.model.TransitStops;
import com.dhara.transitapp.utility.Constants;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

public class ScrollingActivity extends BaseActivity implements IFragmentLoadListener{
    private GoogleMap mGoogleMap;
    private SupportMapFragment mSuppportMapFrag;
    private ViewPager mPagerView;
    private MyPagerAdapter mPageAdapter;
    private IFragmentLoadListener mListener;
    private int mPosition;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        initToolbar();
        initView();
    }

    private void initToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initView(){
        mListener = this;

        if(getIntent().getExtras() != null) {
            mPosition = getIntent().getExtras().getInt(Constants.INTENT_POSITION);
        }

        // get the map
        mSuppportMapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment);
        initMap();

        mPagerView = (ViewPager)findViewById(R.id.viewPager);
        mPageAdapter = new MyPagerAdapter(getSupportFragmentManager(), mListener, mPosition);
        mPagerView.setOffscreenPageLimit(1);
        mPagerView.setAdapter(mPageAdapter);

        mPagerView.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                mPosition = position;
                onFragmentLoaded(mPosition);
                mPagerView.setCurrentItem(mPosition);
            }
        });

        mPagerView.setCurrentItem(mPosition);
    }

    private void initMap(){
        if(mSuppportMapFrag != null) {
            mSuppportMapFrag.getMapAsync(new OnMapReadyCallback() {
                @Override public void onMapReady(GoogleMap googleMap) {
                    if (googleMap != null) {
                        googleMap.getUiSettings().setAllGesturesEnabled(true);
                        mGoogleMap = googleMap;
                    }
                }
            });
        }
    }

    @Override
    public void onFragmentLoaded(int position) {
        initMap();

        if(mGoogleMap != null) {
            mGoogleMap.clear();
        }

        if(position != -1) {
            // load the map fragment with a new polyline color
            TransitRoutes route = SessionManager.getInstance().getTransitHolder().getRouteList().get(position);
            LatLng latLngStart = null;

            for(Segments segment : route.getSegmentList()) {
                PolylineOptions lineOptions = new PolylineOptions()
                        .width(5)
                        .color(Color.parseColor(segment.getColor()));

                for(TransitStops stops : segment.getStopList()) {
                    if(mGoogleMap != null) {
                        latLngStart = new LatLng(stops.getLatitude(), stops.getLongitude());
                        lineOptions.add(latLngStart);
                    }
                }

                if(mGoogleMap != null) {
                    Polyline line = mGoogleMap.addPolyline(lineOptions);
                }
            }

            if(latLngStart != null) {
                CameraPosition cameraPosition = new CameraPosition.Builder().target(latLngStart).zoom(12.0f).build();
                CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                mGoogleMap.moveCamera(cameraUpdate);
            }
        }
    }
}
