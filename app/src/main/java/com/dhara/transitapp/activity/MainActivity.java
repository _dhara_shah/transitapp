package com.dhara.transitapp.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.dhara.transitapp.R;
import com.dhara.transitapp.adapter.TransitAdapter;
import com.dhara.transitapp.asynctask.GetTransitDataAsyncTask;
import com.dhara.transitapp.callback.IResponseListener;
import com.dhara.transitapp.manager.SessionManager;
import com.dhara.transitapp.model.TransitHolder;
import com.dhara.transitapp.model.TransitRoutes;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 25/01/2016.
 */
public class MainActivity extends BaseActivity implements IResponseListener{
    private RecyclerView mTransitList;
    private IResponseListener mListener;
    private TransitAdapter mTransitAdapter;
    private List<TransitRoutes> mRouteList;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initToolbar();
        initView();
        initCall();
    }

    private void initToolbar(){
        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(getString(R.string.app_name));
    }

    private void initView() {
        mListener = this;
        mTransitList = (RecyclerView) findViewById(R.id.transitList);
        mTransitList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mTransitList.setLayoutManager(llm);

        mRouteList = new ArrayList<>();
        mTransitAdapter = new TransitAdapter(this, mRouteList);
        mTransitList.setAdapter(mTransitAdapter);
    }

    private void initCall(){
        new GetTransitDataAsyncTask(mListener).execute();
    }

    @Override
    public void onResponseReceived(Object object) {
        if(object != null) {
            if(object instanceof TransitHolder) {
                TransitHolder holder = (TransitHolder)object;

                // store the entire transit details in a session holder
                // for quick access of data
                // as it would be needed in the next screens also
                SessionManager.getInstance().setTransitHolder(holder);

                mRouteList.addAll(holder.getRouteList());
                mTransitAdapter.notifyDataSetChanged();
            }
        }
    }
}
