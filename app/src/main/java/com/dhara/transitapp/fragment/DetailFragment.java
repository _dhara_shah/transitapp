package com.dhara.transitapp.fragment;

import android.graphics.Color;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.caverock.androidsvg.SVG;
import com.dhara.transitapp.R;
import com.dhara.transitapp.callback.IFragmentLoadListener;
import com.dhara.transitapp.manager.SessionManager;
import com.dhara.transitapp.model.Segments;
import com.dhara.transitapp.model.TransitRoutes;
import com.dhara.transitapp.model.TransitStops;
import com.dhara.transitapp.utility.Constants;
import com.dhara.transitapp.utility.image.SvgDecoder;
import com.dhara.transitapp.utility.image.SvgDrawableTranscoder;
import com.dhara.transitapp.utility.image.SvgSoftwareLayerSetter;

import org.joda.time.DateTime;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * Created by user on 27/01/2016.
 *
 */
public class DetailFragment extends Fragment{
    private static DetailFragment mFragment;
    private TextView mTxtType;
    private TextView mTxtFare;
    private TextView mTxtTimeSlot;
    private TextView mTxtPoweredBy;
    private ImageView mImgPoweredBy;
    private View mView;
    private int mPosition;
    private LinearLayout mLinDynamicImages;
    private LinearLayout mLinDynamicStops;
    private IFragmentLoadListener mListener;
    private GenericRequestBuilder<Uri, InputStream, SVG, PictureDrawable> requestBuilder;

    public void setListener(IFragmentLoadListener listener){
        mListener = listener;
    }

    public static DetailFragment newInstance(int position) {
        mFragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.INTENT_POSITION, position);
        mFragment.setArguments(args);
        return mFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_detail, container, false);
        Log.e("dhara","onCreateView");
        initView();
        return mView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser == false) {
            System.out.println("ACCEPT_FALSE");
        }else{
            System.out.println("ACCEPT_TRUE");

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    loadData();
                }
            }, 5);
        }
    }

    private void initView(){
        requestBuilder = Glide.with(getActivity())
                .using(Glide.buildStreamModelLoader(Uri.class, getActivity()), InputStream.class)
                .from(Uri.class)
                .as(SVG.class)
                .transcode(new SvgDrawableTranscoder(), PictureDrawable.class)
                .sourceEncoder(new StreamEncoder())
                .cacheDecoder(new FileToStreamDecoder<SVG>(new SvgDecoder()))
                .decoder(new SvgDecoder())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .animate(android.R.anim.fade_in)
                .listener(new SvgSoftwareLayerSetter<Uri>());

        mTxtTimeSlot = (TextView)mView.findViewById(R.id.txtTimeSlot);
        mTxtType = (TextView)mView.findViewById(R.id.txtType);
        mTxtFare = (TextView)mView.findViewById(R.id.txtFare);
        mLinDynamicStops = (LinearLayout)mView.findViewById(R.id.linDynamicStops);
        mLinDynamicImages = (LinearLayout)mView.findViewById(R.id.lnrDynamicModes);
        mTxtPoweredBy = (TextView)mView.findViewById(R.id.txtPoweredBy);
        mImgPoweredBy = (ImageView)mView.findViewById(R.id.imgPoweredBy);

        mPosition = (getArguments() != null) ? (getArguments().getInt(Constants.INTENT_POSITION)) : -1;
    }

    private void loadData() {
        if(mPosition != -1) {
            // load data here
            TransitRoutes route = SessionManager.getInstance().getTransitHolder().getRouteList().get(mPosition);
            setPoweredBy(route);

            mTxtType.setText(route.getType());
            mTxtFare.setText((route.getPrice() != null) ?
                    route.getPrice().getCurrency() + " " + route.getPrice().getAmount() : "");

            SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);

            Set<String> setOfDestinations = new HashSet<>();
            int i= 0, j=0;

            mLinDynamicImages.removeAllViews();
            mLinDynamicStops.removeAllViews();

            for(Segments segment : route.getSegmentList()) {
                LayoutInflater imageInflater = LayoutInflater.from(getActivity());
                View imageView = imageInflater.inflate(R.layout.individual_image_row, null);
                ImageView img = (ImageView)imageView.findViewById(R.id.imgMode);
                img.setBackgroundColor(Color.parseColor(route.getSegmentList().get(i).getColor()));

                Uri uri = Uri.parse(route.getSegmentList().get(i).getIconURL());
                requestBuilder
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        // SVG cannot be serialized so it's not worth to cache it
                        .load(uri)
                        .into(img);

                mLinDynamicImages.addView(imageView);
                mLinDynamicImages.invalidate();

                for(TransitStops stops : segment.getStopList()) {
                    String stopName = "";
                    if(!TextUtils.isEmpty(stops.getName())) {
                        stopName = stops.getName();
                    }else {
                        stopName = "Start Location";
                    }

                    DateTime dateTimeFirst = new DateTime(stops.getDatetime());
                    java.util.Date date = dateTimeFirst.toDate();

                    // get the first start point
                    if(i==0 && j == 0) {
                        mTxtTimeSlot.setText(sdfTime.format(date));
                    }else if((i==route.getSegmentList().size() - 1) &&
                            (j == segment.getStopList().size()-1)) {
                        mTxtTimeSlot.append(" --> ");
                        mTxtTimeSlot.append(sdfTime.format(date));
                    }

                    if(!setOfDestinations.contains(stopName)) {
                        LayoutInflater inflater = LayoutInflater.from(getActivity());
                        View view = inflater.inflate(R.layout.individual_segments_row, null);
                        TextView txtName= (TextView)view.findViewById(R.id.txtName);
                        TextView txtTime = (TextView)view.findViewById(R.id.txtTime);
                        View viewBg = (View)view.findViewById(R.id.viewBg);
                        txtName.setText(stopName);
                        viewBg.setBackgroundColor(Color.parseColor(segment.getColor()));
                        setOfDestinations.add(stopName);
                        txtTime.setText(sdfTime.format(date));

                        mLinDynamicStops.addView(view);
                        mLinDynamicStops.invalidate();
                    }

                    j++;
                }
                i++;
            }

            if(mListener != null) {mListener.onFragmentLoaded(mPosition);}
        }
    }

    private void setPoweredBy(TransitRoutes route) {
        String imageURL = "";
        String provider = "";

        if(route.getProvider().equals("vbb")) {
            provider = SessionManager.getInstance().getTransitHolder().getProviderHolder().getVbb().getDisclaimer();
            imageURL = SessionManager.getInstance().getTransitHolder().getProviderHolder().getVbb().getProviderIconUrl();
        }else if(route.getProvider().equals("drivenow")) {
            provider = SessionManager.getInstance().getTransitHolder().getProviderHolder().getDrivenow().getDisclaimer();
            imageURL = SessionManager.getInstance().getTransitHolder().getProviderHolder().getDrivenow().getProviderIconUrl();
        }else if(route.getProvider().equals("drivenow")) {
            provider = SessionManager.getInstance().getTransitHolder().getProviderHolder().getDrivenow().getDisclaimer();
            imageURL = SessionManager.getInstance().getTransitHolder().getProviderHolder().getDrivenow().getProviderIconUrl();
        }else if(route.getProvider().equals("car2go")) {
            provider = SessionManager.getInstance().getTransitHolder().getProviderHolder().getCar2go().getDisclaimer();
            imageURL = SessionManager.getInstance().getTransitHolder().getProviderHolder().getCar2go().getProviderIconUrl();
        }else if(route.getProvider().equals("google")) {
            provider = SessionManager.getInstance().getTransitHolder().getProviderHolder().getGoogle().getDisclaimer();
            imageURL = SessionManager.getInstance().getTransitHolder().getProviderHolder().getGoogle().getProviderIconUrl();
        }else if(route.getProvider().equals("nextbike")) {
            provider = SessionManager.getInstance().getTransitHolder().getProviderHolder().getNextbike().getDisclaimer();
            imageURL = SessionManager.getInstance().getTransitHolder().getProviderHolder().getNextbike().getProviderIconUrl();
        }else if(route.getProvider().equals("callabike")) {
            provider = SessionManager.getInstance().getTransitHolder().getProviderHolder().getCallabike().getDisclaimer();
            imageURL = SessionManager.getInstance().getTransitHolder().getProviderHolder().getCallabike().getProviderIconUrl();
        }

        mTxtPoweredBy.setText(provider);
        Uri uri = Uri.parse(imageURL);
        requestBuilder
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                // SVG cannot be serialized so it's not worth to cache it
                .load(uri)
                .into(mImgPoweredBy);
    }

}
