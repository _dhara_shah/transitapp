package com.dhara.transitapp.manager;

import com.dhara.transitapp.model.TransitHolder;

/**
 * Created by USER on 26-01-2016.
 */
public class SessionManager {
    private static SessionManager sessionManager;
    private TransitHolder transitHolder;

    public static SessionManager getInstance() {
        if(sessionManager == null) {
            sessionManager = new SessionManager();
        }
        return sessionManager;
    }

    public TransitHolder getTransitHolder() {
        return transitHolder;
    }

    public void setTransitHolder(TransitHolder transitHolder) {
        this.transitHolder = transitHolder;
    }
}
