package com.dhara.transitapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by USER on 26-01-2016.
 */
public class TransitHolder {
    @SerializedName("routes")
    private List<TransitRoutes> routeList;
    @SerializedName("provider_attributes")
    private ProviderHolder providerHolder;

    public List<TransitRoutes> getRouteList() {
        return routeList;
    }
    public void setRouteList(List<TransitRoutes> routeList) {
        this.routeList = routeList;
    }

    public ProviderHolder getProviderHolder() {
        return providerHolder;
    }

    public void setProviderHolder(ProviderHolder providerHolder) {
        this.providerHolder = providerHolder;
    }
}
