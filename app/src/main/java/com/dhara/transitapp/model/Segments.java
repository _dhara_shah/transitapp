package com.dhara.transitapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by USER on 26-01-2016.
 */
public class Segments {
    private String name;
    @SerializedName("num_stops")
    private int numberOfStops;
    @SerializedName("stops")
    private List<TransitStops> stopList;
    @SerializedName("travel_mode")
    private String travelMode;
    private String description;
    private String color;
    @SerializedName("icon_url")
    private String iconURL;
    private String polyline;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfStops() {
        return numberOfStops;
    }

    public void setNumberOfStops(int numberOfStops) {
        this.numberOfStops = numberOfStops;
    }

    public List<TransitStops> getStopList() {
        return stopList;
    }

    public void setStopList(List<TransitStops> stopList) {
        this.stopList = stopList;
    }

    public String getTravelMode() {
        return travelMode;
    }

    public void setTravelMode(String travelMode) {
        this.travelMode = travelMode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getIconURL() {
        return iconURL;
    }

    public void setIconURL(String iconURL) {
        this.iconURL = iconURL;
    }

    public String getPolyline() {
        return polyline;
    }

    public void setPolyline(String polyline) {
        this.polyline = polyline;
    }
}
