package com.dhara.transitapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by USER on 26-01-2016.
 */
public class TransitRoutes {
    @SerializedName("type")
    private String type;
    @SerializedName("provider")
    private String provider;
    @SerializedName("segments")
    private List<Segments> segmentList;
    private TransitProperties properties;
    private PriceModel price;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public List<Segments> getSegmentList() {
        return segmentList;
    }

    public void setSegmentList(List<Segments> segmentList) {
        this.segmentList = segmentList;
    }

    public TransitProperties getProperties() {
        return properties;
    }

    public void setProperties(TransitProperties properties) {
        this.properties = properties;
    }

    public PriceModel getPrice() {
        return price;
    }

    public void setPrice(PriceModel price) {
        this.price = price;
    }
}
