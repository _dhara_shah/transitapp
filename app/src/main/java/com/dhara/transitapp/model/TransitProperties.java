package com.dhara.transitapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by USER on 26-01-2016.
 */
public class TransitProperties {
    private String id;
    @SerializedName("available_bikes")
    private int availableBikes;
    private String address;
    private String model;
    @SerializedName("license_plate")
    private String licensePlate;
    @SerializedName("fuel_level")
    private int fuelLevel;
    @SerializedName("engine_type")
    private String engineType;
    @SerializedName("internal_cleanliness")
    private String internalCleanliness;
    private String description;
    private String seats;
    private String doors;
    @SerializedName("companies")
    private List<TaxiCompany> taxiCompanyList;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public int getFuelLevel() {
        return fuelLevel;
    }

    public void setFuelLevel(int fuelLevel) {
        this.fuelLevel = fuelLevel;
    }

    public String getEngineType() {
        return engineType;
    }

    public void setEngineType(String engineType) {
        this.engineType = engineType;
    }

    public String getInternalCleanliness() {
        return internalCleanliness;
    }

    public void setInternalCleanliness(String internalCleanliness) {
        this.internalCleanliness = internalCleanliness;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSeats() {
        return seats;
    }

    public void setSeats(String seats) {
        this.seats = seats;
    }

    public String getDoors() {
        return doors;
    }

    public void setDoors(String doors) {
        this.doors = doors;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getAvailableBikes() {
        return availableBikes;
    }

    public void setAvailableBikes(int availableBikes) {
        this.availableBikes = availableBikes;
    }
}
