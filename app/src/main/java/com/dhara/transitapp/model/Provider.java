package com.dhara.transitapp.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by USER on 26-01-2016.
 */
public class Provider {
    @SerializedName("provider_icon_url")
    private String providerIconUrl;
    @SerializedName("disclaimer")
    private String disclaimer;
    @SerializedName("ios_itunes_url")
    private String iTunesUrl;
    @SerializedName("ios_app_url")
    private String iosAppUrl;
    @SerializedName("android_package_name")
    private String androidPackageName;
    @SerializedName("display_name")
    private String displayName;

    public String getProviderIconUrl() {
        return providerIconUrl;
    }

    public void setProviderIconUrl(String providerIconUrl) {
        this.providerIconUrl = providerIconUrl;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    public String getiTunesUrl() {
        return iTunesUrl;
    }

    public void setiTunesUrl(String iTunesUrl) {
        this.iTunesUrl = iTunesUrl;
    }

    public String getIosAppUrl() {
        return iosAppUrl;
    }

    public void setIosAppUrl(String iosAppUrl) {
        this.iosAppUrl = iosAppUrl;
    }

    public String getAndroidPackageName() {
        return androidPackageName;
    }

    public void setAndroidPackageName(String androidPackageName) {
        this.androidPackageName = androidPackageName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}