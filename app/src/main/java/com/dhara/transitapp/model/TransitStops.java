package com.dhara.transitapp.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by USER on 26-01-2016.
 */
public class TransitStops {
    @SerializedName("lat")
    private double latitude;
    @SerializedName("lng")
    private double longitude;
    private String datetime;
    private String name;
    private String properties;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProperties() {
        return properties;
    }

    public void setProperties(String properties) {
        this.properties = properties;
    }
}
