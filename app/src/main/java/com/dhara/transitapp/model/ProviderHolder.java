package com.dhara.transitapp.model;

/**
 * Created by USER on 26-01-2016.
 */
public class ProviderHolder {
    private Provider vbb;
    private Provider drivenow;
    private Provider car2go;
    private Provider google;
    private Provider nextbike;
    private Provider callabike;

    public Provider getVbb() {
        return vbb;
    }

    public void setVbb(Provider vbb) {
        this.vbb = vbb;
    }

    public Provider getDrivenow() {
        return drivenow;
    }

    public void setDrivenow(Provider drivenow) {
        this.drivenow = drivenow;
    }

    public Provider getCar2go() {
        return car2go;
    }

    public void setCar2go(Provider car2go) {
        this.car2go = car2go;
    }

    public Provider getGoogle() {
        return google;
    }

    public void setGoogle(Provider google) {
        this.google = google;
    }

    public Provider getNextbike() {
        return nextbike;
    }

    public void setNextbike(Provider nextbike) {
        this.nextbike = nextbike;
    }

    public Provider getCallabike() {
        return callabike;
    }

    public void setCallabike(Provider callabike) {
        this.callabike = callabike;
    }
}
