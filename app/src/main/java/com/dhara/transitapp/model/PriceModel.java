package com.dhara.transitapp.model;

/**
 * Created by USER on 26-01-2016.
 */
public class PriceModel {
    private String currency;
    private float amount;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }
}
