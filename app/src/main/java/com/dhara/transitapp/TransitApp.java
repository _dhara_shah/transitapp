package com.dhara.transitapp;

import android.app.Application;
import android.content.Context;

/**
 * Created by user on 25/01/2016.
 */
public class TransitApp extends Application {
    private static Context mContext;
    private static TransitApp mApp;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        mApp = this;
    }

    public static TransitApp getAppContext() {
        if(mApp == null) {
            mApp = (TransitApp)mContext;
        }
        return mApp;
    }
}
