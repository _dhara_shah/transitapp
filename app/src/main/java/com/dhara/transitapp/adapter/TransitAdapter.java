package com.dhara.transitapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.caverock.androidsvg.SVG;
import com.dhara.transitapp.R;
import com.dhara.transitapp.TransitApp;
import com.dhara.transitapp.activity.ScrollingActivity;
import com.dhara.transitapp.model.TransitRoutes;
import com.dhara.transitapp.model.TransitStops;
import com.dhara.transitapp.utility.Constants;
import com.dhara.transitapp.utility.image.SvgDecoder;
import com.dhara.transitapp.utility.image.SvgDrawableTranscoder;
import com.dhara.transitapp.utility.image.SvgSoftwareLayerSetter;

import org.joda.time.DateTime;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by USER on 26-01-2016.
 */
public class TransitAdapter extends RecyclerView.Adapter<SimpleViewHolder> {
    private List<TransitRoutes> mRouteList;
    private int mCurrentId, mLastId = 0;
    private Context mContext;
    private GenericRequestBuilder<Uri, InputStream, SVG, PictureDrawable> requestBuilder;

    public TransitAdapter(Context context, List<TransitRoutes> routes) {
        mRouteList = routes;
        mContext = context;

        requestBuilder = Glide.with(mContext)
                .using(Glide.buildStreamModelLoader(Uri.class, mContext), InputStream.class)
                .from(Uri.class)
                .as(SVG.class)
                .transcode(new SvgDrawableTranscoder(), PictureDrawable.class)
                .sourceEncoder(new StreamEncoder())
                .cacheDecoder(new FileToStreamDecoder<SVG>(new SvgDecoder()))
                .decoder(new SvgDecoder())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .animate(android.R.anim.fade_in)
                .listener(new SvgSoftwareLayerSetter<Uri>());
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.individual_transit_row, viewGroup, false);
        return new SimpleViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder simpleViewHolder, int position) {
        TransitRoutes route = mRouteList.get(position);
        simpleViewHolder.txtType.setText(route.getType());
        simpleViewHolder.txtFare.setText((route.getPrice() != null) ?
                (route.getPrice().getCurrency() + " " + route.getPrice().getAmount()) : "");

        for(int i=0;i<route.getSegmentList().size();i++){
            LayoutInflater inflater = LayoutInflater.from(mContext);
            View itemView = inflater.inflate(R.layout.individual_image_row, null);
            ImageView img = (ImageView)itemView.findViewById(R.id.imgMode);
            img.setBackgroundColor(Color.parseColor(route.getSegmentList().get(i).getColor()));

            Uri uri = Uri.parse(route.getSegmentList().get(i).getIconURL());
            requestBuilder
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    // SVG cannot be serialized so it's not worth to cache it
                    .load(uri)
                    .into(img);

            simpleViewHolder.linDynamicImages.addView(itemView);
            simpleViewHolder.linDynamicImages.invalidate();
        }

        if(route.getSegmentList().size() > 0) {
            TransitStops firstStop = route.getSegmentList().get(0).getStopList().get(0);
            List<TransitStops> lastStops = route.getSegmentList().get(route.getSegmentList().size() -1).getStopList();
            TransitStops lastStop = lastStops.get(lastStops.size() - 1);

            DateTime dateTimeFirst = new DateTime(firstStop.getDatetime());
            java.util.Date date = dateTimeFirst.toDate();
            System.out.println("Date :: " + date);

            SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
            simpleViewHolder.txtTimeDuration.setText(sdfTime.format(date));

            simpleViewHolder.txtTimeDuration.append(" --> ");

            DateTime dateTimeStop = new DateTime(lastStop.getDatetime());
            java.util.Date dateStop = dateTimeStop.toDate();
            simpleViewHolder.txtTimeDuration.append(sdfTime.format(dateStop));
        }


        simpleViewHolder.linWholeView.setTag(String.valueOf(position));
        simpleViewHolder.linWholeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout linLayout = (LinearLayout)view;
                int pos = Integer.parseInt((String)linLayout.getTag());

                Log.e("dhara","clicked on ");

                Intent intent = new Intent(TransitApp.getAppContext(),
                        ScrollingActivity.class);
                intent.putExtra(Constants.INTENT_POSITION, pos);
                ((Activity)mContext).startActivity(intent);
                ((Activity)mContext).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        mCurrentId = position;
        Animation animation = AnimationUtils.loadAnimation(TransitApp.getAppContext(),
                (mCurrentId > mLastId) ? R.anim.up_from_bottom : R.anim.down_from_top);
        simpleViewHolder.cardView.startAnimation(animation);
        mLastId = mCurrentId;
    }

    @Override
    public void onViewRecycled(SimpleViewHolder simpleViewHolder) {
        super.onViewRecycled(simpleViewHolder);
        simpleViewHolder.linDynamicImages.removeAllViews();
    }

    @Override
    public int getItemCount() {
        return mRouteList.size();
    }
}
