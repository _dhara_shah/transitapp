package com.dhara.transitapp.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.dhara.transitapp.R;

/**
 * Created by USER on 26-01-2016.
 */
public class SimpleViewHolder extends RecyclerView.ViewHolder {
    public TextView txtFare;
    public TextView txtTimeDuration;
    public TextView txtType;
    public LinearLayout linDynamicImages;
    public LinearLayout linWholeView;
    public CardView cardView;

    public SimpleViewHolder(View itemView) {
        super(itemView);
        txtFare = (TextView)itemView.findViewById(R.id.txtFare);
        txtTimeDuration = (TextView)itemView.findViewById(R.id.txtTimeSlot);
        txtType = (TextView)itemView.findViewById(R.id.txtType);
        linDynamicImages = (LinearLayout)itemView.findViewById(R.id.lnrDynamicModes);
        linWholeView = (LinearLayout) itemView.findViewById(R.id.linWholeView);
        cardView = (CardView)itemView.findViewById(R.id.card_view);
    }
}
