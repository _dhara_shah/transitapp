package com.dhara.transitapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.dhara.transitapp.callback.IFragmentLoadListener;
import com.dhara.transitapp.fragment.DetailFragment;
import com.dhara.transitapp.manager.SessionManager;

/**
 * Created by user on 27/01/2016.
 */
public class MyPagerAdapter extends FragmentStatePagerAdapter {
    private static int COUNT;
    private int mPosition;
    private IFragmentLoadListener mListener;

    public MyPagerAdapter(FragmentManager fm, IFragmentLoadListener listener, int position) {
        super(fm);
        COUNT = SessionManager.getInstance().getTransitHolder().getRouteList().size();
        mPosition = position;
        mListener = listener;
    }

    @Override
    public Fragment getItem(int position) {
        DetailFragment fragment = DetailFragment.newInstance(position);
        Log.e("dhara","position >> " + position);
        fragment.setListener(mListener);
        return fragment;
    }


    @Override
    public int getCount() {
        return COUNT;
    }
}
