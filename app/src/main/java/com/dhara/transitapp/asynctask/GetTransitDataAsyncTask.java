package com.dhara.transitapp.asynctask;

import android.os.AsyncTask;

import com.dhara.transitapp.TransitApp;
import com.dhara.transitapp.callback.IResponseListener;
import com.dhara.transitapp.model.TransitHolder;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by USER on 26-01-2016.
 */
public class GetTransitDataAsyncTask extends AsyncTask<Void,Void, TransitHolder> {
    private IResponseListener mListener;
    public GetTransitDataAsyncTask(IResponseListener responseListener) {
        mListener = responseListener;
    }

    @Override
    protected TransitHolder doInBackground(Void... voids) {
        try {
            InputStream is = TransitApp.getAppContext().getAssets().open("data.json");
            BufferedReader reader = new BufferedReader(new InputStreamReader(is,"UTF-8"));
            Gson gson = new Gson();
            TransitHolder holder = gson.fromJson(reader, TransitHolder.class);
            reader.close();
            is.close();
            return holder;
        }catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(TransitHolder transitHolder) {
        super.onPostExecute(transitHolder);
        if(mListener != null) mListener.onResponseReceived(transitHolder);
    }
}
