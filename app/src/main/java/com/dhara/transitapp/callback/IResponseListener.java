package com.dhara.transitapp.callback;

/**
 * Created by USER on 26-01-2016.
 */
public interface IResponseListener {
    void onResponseReceived(Object object);
}
