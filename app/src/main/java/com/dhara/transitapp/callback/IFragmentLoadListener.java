package com.dhara.transitapp.callback;

/**
 * Created by user on 28/01/2016.
 */
public interface IFragmentLoadListener {
    void onFragmentLoaded(int position);
}
