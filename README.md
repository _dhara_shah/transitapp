# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* TransitApp is a small application that displays all the transit details and modes from one location to another. This is not a live app and hence does not deal with live data
* 1.0

### How do I get set up? ###

* Clone this repository or import this repository via Git in Android Studio. 
* JDK 1.8 is used but JDK 1.7 can also be used for the same
* There are no extra dependencies in this project
* No database is involved
* Build the application and find the apk in the folder /app/build/outputs/apk/

# Features #

* Transit option list
* Route map with stations that would be covered in the transit

# Known issues #

* This application does not have a very well set UI/UX
* The icons for the transit modes are not well set either
* When we have a station repeating due to changes in a transit mode, the same is not reflected in the colors. That means the latest station entry will override the color of the previous entry under the same name.

### Who do I talk to? ###

* Dhara Shah
* sdhara2@hotmail.com